public class SavePatientsHangoutURLMapping {
    public String UpdateRecord{get;set;}
    public SavePatientsHangoutURLMapping() {
       UpdateRecord = null;
    }
    
    public void SaveMappig() {
        try {
            String URLValue = ApexPages.currentPage().getParameters().get('HangoutURLAsURLParameter');
            String SelectedRecordId = ApexPages.currentPage().getParameters().get('LaunchForOppId');
            String SelectedConsultationId = ApexPages.currentPage().getParameters().get('PassedConsultationId');
            if(SelectedRecordId != null && SelectedRecordId != 'undefined' && SelectedRecordId != '' && URLValue != null && URLValue != '') {
               PatientsHangoutURLMapping__c TempObj = new PatientsHangoutURLMapping__c();
               TempObj.HangoutURL__c = URLValue;
               TempObj.Posted__c = false;
               TempObj.Record_Id__c = SelectedRecordId;
			   TempObj.Consultation_Id__c = SelectedConsultationId;
               insert TempObj;
               UpdateRecord = 'Invitation has been sent to Doctors';
            }
        }
        catch(exception ex) {
        }
    }
}