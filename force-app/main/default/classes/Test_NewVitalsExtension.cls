@IsTest
public class Test_NewVitalsExtension {
    public static testmethod void NewVitals() {
        // Put Id into the current page Parameters
        
        Contact p = new Contact();
        p.LastName = 'Patient';
        p.Gender__c = 'Male';
        p.Birthdate = system.today();
        insert p ;
        
        Vitals__c v = new Vitals__c();
        v.Patient__c = p.id;
        v.Height_cms__c = 165;
        v.Height_Unit__c = 'cm';
        v.Weight_kgs__c = 60;
        v.Weight_Unit__c = 'kg';
        insert v;
        
        ApexPages.StandardController beforeController = new ApexPages.StandardController(v);
        NewVitalsExtension beforeExtension = new NewVitalsExtension(beforeController);
		ApexPages.currentPage().getParameters().put('edit', 'true');
        ApexPages.currentPage().getParameters().put('Id', v.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(v);
		
        Test.startTest();
        NewVitalsExtension extension = new NewVitalsExtension(controller);
        extension.save();
        //c.NewVitalsExtension();
        Test.stopTest();
    }
}