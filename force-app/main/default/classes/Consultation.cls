public class Consultation {
    public Id patientId  = apexPages.currentPage().getParameters().get('Id');
    public Id consultationId  = apexPages.currentPage().getParameters().get('consultationId');
    public Id visitId = apexPages.currentPage().getParameters().get('visitId');
    public string patientName {get;set;}
    public string patientGender {get;set;}
    public date patientDOB {get;set;}
    public List<Patient_Allergy__c> allergies;
    public List<Patient_Diagnosis__c> patientDiagnosis;
    public List<Patient_Diagnosis__c> patientProblemList;
    public List<Patient_Medicine__c> patientMedication;
    public List<Lab_Report__c> patientlabData;
    public List<Vitals__c> patientVital;
    public List<Attachment> patientDocuments;
    public List<Contact> patientInfo {get;set;}
    private final Contact con;
    SOAP_Note__c SOAPNote;
    public List<SOAP_Note__c> patientSOAPNotes {get;set;}
    
    public consultation() {
    }
    
    public consultation(ApexPages.StandardController stdController) {
        this.con = (Contact)stdController.getRecord();
        patientInfo = [Select Name, Gender__c, Birthdate from Contact where Id=: patientId ];
        patientName = patientInfo[0].Name;
        patientGender = patientInfo[0].Gender__c;
        patientDOB = patientInfo[0].Birthdate;
    }
    
    public SOAP_Note__c getSOAPNote() {
        if(SOAPNote == null) SOAPNote = new SOAP_Note__c();
        return SOAPNote;
    }
    
    public List<SOAP_Note__c> getSOAPNotes() {
        if(patientSOAPNotes == null) 
            patientSOAPNotes = [select Id, Name, CreatedDate, Soap_Assessment__c from SOAP_Note__c where Visit__r.Patient__c =: patientId Order By CreatedDate Desc];
        return patientSOAPNotes;
    }  
    
    public List<Attachment> getAttachments() {
        if(patientDocuments == null) 
            patientDocuments = [select Id, Name, CreatedDate from Attachment where ParentId =: patientId];
        return patientDocuments;
    }
    
    public List<Patient_Allergy__c> getAllergies() {
        if(allergies == null) 
            allergies = [select Id, Allergy__r.Name from Patient_Allergy__c where Patient__c =: patientId];
        return allergies;
    }
    
    public List<Patient_Diagnosis__c> getDiagnosis() {
        if(patientDiagnosis == null) 
            patientDiagnosis = [select Id, Diagnosis__r.Name, Diagnosis__r.Description__c from Patient_Diagnosis__c where Patient__c =: patientId and Active__c = false];
        return patientDiagnosis;
    }
    
    public List<Patient_Diagnosis__c> getProblemList() {
        if(patientProblemList == null) 
            patientProblemList = [select Id, Diagnosis__r.Name, Diagnosis__r.Description__c from Patient_Diagnosis__c where Patient__c =: patientId and Active__c = true];
        return patientProblemList;
    }
    
    public List<Patient_Medicine__c> getMedication() {
        if(patientMedication == null) 
            patientMedication = [select Id, Medicine_List__r.Name, Dose__c, Frequency__c from Patient_Medicine__c where Patient__c =: patientId];
        return patientMedication;
    }
    
    public List<Lab_Report__c> getlabReport() {
        if(patientlabData == null) 
            patientlabData = [select Id, RecordType.Name from Lab_Report__c where Patient__c =: patientId];
        return patientlabData;
    }
    
    public List<Vitals__c> getVitalRecord() {
        if(patientVital == null) 
            patientVital = [select Id, Name, BP_Diastolic__c, BP_Systolic__c, Case__c, Heart_Rate_bits_min__c,
                            Height_cms__c, Patient__c, Patient_Full_Name__c, Temp_F__c, Weight_kgs__c, CreatedDate
                            from Vitals__c where Patient__c =: patientId order by createddate desc];
        return patientVital;
    }
    
    public PageReference saveSOAPNotes() {
        SOAPNote.Visit__c = visitId;
        insert SOAPNote;    
        PageReference homePage;
        homePage = new PageReference('/home/home.jsp' );
        return homePage.setRedirect(true);    
    }
}