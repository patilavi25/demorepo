public class Zoom_IntegrationController 
{
    private string key = 'XQpC2QQSRNa9xhh1PGFTaQ' ;
    private string secret = 'H9vzHLllO079rUQfopLDiMZoolJ6XNYH';
    private string redirect_uri = 'https://'+ System.URL.getSalesforceBaseUrl().getHost()+'/apex/zoom_integration';
    string paramvalue='';  
    private String identificationToken ;    
    private String access_token; 

    public string meetingTitle{get;set;}
    public datetime startDateTime{get;set;}
    public integer meetingDuration{get;set;}
    public string meetingPassword{get;set;}
    public Zoom_MeetingObject meetingObj{get;set;}

    
    
    
    public Zoom_IntegrationController()
    {  
        meetingTitle = '';
        startDateTime = datetime.now();
        meetingDuration = 30;
        meetingPassword = '';
        identificationToken = ApexPages.currentPage().getParameters().get('code') ;        
        if(identificationToken != '' && identificationToken != null)
        {
            AccessToken();
        } 
    }
    
    public PageReference ZoomAuth()
    {
       PageReference pg = new PageReference(new Zoom_AuthURIAfterRequest(key , redirect_uri).AuthenticationURI) ;
        return pg;
       
    }
    
    public void AccessToken()
    {

      paramvalue = System.EncodingUtil.base64Encode(Blob.valueof(key+':'+secret));

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://zoom.us/oauth/token');
        String messageBody = 'grant_type=authorization_code&code='+identificationToken+'&redirect_uri='+redirect_uri;
        req.setHeader('Authorization', 'Basic '+paramvalue);
        req.setBody(messageBody);
        req.setTimeout(60*1000);
        
        Http h = new Http();
        HttpResponse res = h.send(req);
        JSONParser parser = JSON.createParser(res.getBody());
        system.debug('Pradeep test >> '+parser);
       while (parser.nextToken() != null) 
        {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token'))
            {
                parser.nextToken();
                access_token=parser.getText();
                break;
            }
        }
        if (access_token!='' && access_token != null)
        {system.debug('Pradeep access_token >> '+access_token);
        //getUserId();
        //setMeeting();
        }
       
    }

    public PageReference getUserId()
    {  
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        req1.setEndpoint('https://api.zoom.us/v2/users?page_number=1&page_size=30&status=active');
        req1.setHeader('authorization', 'Bearer '+access_token);
        req1.setHeader('content-type', 'application/json');

        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('User List Pradeep'+resl.getBody());
        
        return null;
    }
    
    public PageReference setMeeting()
    {  
        system.debug('--startDateTime---'+startDateTime);
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('POST');
        req1.setEndpoint('https://api.zoom.us/v2/users/avinash.patil@bolt.today/meetings');
        req1.setHeader('content-type', 'application/json');
        req1.setHeader('Authorization', 'Bearer '+access_token);
        String messageBody = '{ \"topic\": \"'+meetingTitle+'\", \"start_time\": \"'+startDateTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SS\'Z\'')+'\", \"duration\": '+meetingDuration+',  \"timezone\": \"Asia/Calcutta\", \"password\": \"'+meetingPassword+'\"}';
        System.debug('messageBody Pradeep'+messageBody);
        req1.setBody(messageBody);
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('Meeting Pradeep'+resl.getBody());
        JSONParser parserD = JSON.createParser(resl.getBody());
        system.debug('Pradeep parserD >> '+ parserD);
       
       meetingObj = Zoom_MeetingObject.parse( resl.getBody());
       system.debug('Meeting info >> '+meetingObj);
        return null;
    }
    
}