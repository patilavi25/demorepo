public class getZoomDetails {
 	//public string key = 'kMPzDLh2QNmesFnYoQvipw' ;
    //public string secret = 'vmkcTyxZsLWou6cFMcJ41JInniZmfPti';
    public string key { get; set; }
    public string secret { get; set; }
    //public string key = 'XQpC2QQSRNa9xhh1PGFTaQ' ;
    //public string secret = 'H9vzHLllO079rUQfopLDiMZoolJ6XNYH';
    
    public string redirect_uri = 'https://'+ System.URL.getSalesforceBaseUrl().getHost()+'/apex/getMeetingDetails';
    public string paramvalue='';  
    public String identificationToken ;    
    public String access_token;
    public String consultingId { get; set; }
    Map<ID, Consulting__c> consultingMap {get; set;}
    
    public string meetingTitle{get;set;}
    public datetime startDateTime{get;set;}
    public integer meetingDuration{get;set;}
    public integer approvalType{get;set;}
    public string meetingPassword{get;set;}
    public Zoom_MeetingObject meetingObj{get;set;}
    public List<Consulting__c> consultingList {get;set;}
    public List<zoom_keys__mdt> zoomkeys {get;set;}
  
    public getZoomDetails()
    {  
        zoomkeys = [Select Client_ID__c, Client_Secret__c, DeveloperName From zoom_keys__mdt Where DeveloperName = 'get_zoom_details']; 
        if(zoomkeys.size()>0){
            key = zoomkeys[0].Client_ID__c;
            secret = zoomkeys[0].Client_Secret__c;
        }
        consultingMap = new Map<ID, Consulting__c>([Select id, Name, meetingId__c, Meeting_details_updated__c, Zoom_Meeting_UUID__c, From_Time__c, Zoom_Meeting_Password__c, Patients_Email__c, Visit__r.Patient__c, Patient__c, To_Time__c, User__c, User__r.Name, User__r.Email, User__r.TimeZoneSidKey, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c Where isConfirmed__c=true AND Start_Time__c <= today AND Visit__c != null AND Meeting_details_updated__c=false]); 
        //consultingList = [Select id, Name, From_Time__c, Visit__r.Patient__c, Patient__c, To_Time__c, User__c, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c where isConfirmed__c = false AND Visit__c != null]; 
        system.debug('--consultingMap--'+consultingMap);
        consultingList = consultingMap.values();
        meetingTitle = '';
        startDateTime = datetime.now();
        meetingDuration = 30;
        approvalType=0;
        meetingPassword = '';
        identificationToken = ApexPages.currentPage().getParameters().get('code') ; 
        system.debug('--identificationToken--'+identificationToken);
        if(access_token=='' || access_token==null){
            if(identificationToken != '' && identificationToken != null)
            {
                AccessToken();
                system.debug('--identificationToken--'+identificationToken+'--access_token--'+access_token);
            }  
        } 
    }
    
    public PageReference ZoomAuth(){	
        system.debug('**access_token**'+access_token);
        if(access_token=='' || access_token==null){
            PageReference pg = new PageReference(new Zoom_AuthURIAfterRequest(key , redirect_uri).AuthenticationURI) ;
            return pg; 
        }
        return null;
    }
    
    public void AccessToken()
    {
        system.debug('==code=='+identificationToken);
        paramvalue = System.EncodingUtil.base64Encode(Blob.valueof(key+':'+secret));
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://zoom.us/oauth/token');
        String messageBody = 'grant_type=authorization_code&code='+identificationToken+'&redirect_uri='+redirect_uri;
        req.setHeader('Authorization', 'Basic '+paramvalue);
        req.setBody(messageBody);
        req.setTimeout(60*1000);
        
        Http h = new Http();
        HttpResponse res;
        if(!Test.isRunningTest()){
            res = h.send(req);
            
            system.debug(res.getBody());
            JSONParser parser = JSON.createParser(res.getBody());
            system.debug('Avinash test >> '+parser);
            if(res.getStatusCode()==400){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please click Update Meeting Details tab again to refresh the token'));
                return;
            }    
            while (parser.nextToken() != null) 
            {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token'))
                {
                    parser.nextToken();
                    access_token=parser.getText();
                    break;
                }
            }}
        if (access_token!='' && access_token != null)
        {system.debug('Avinash access_token >> '+access_token);
        }     
    }
    
    /*public PageReference getUserId()
    {  
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        req1.setEndpoint('https://api.zoom.us/v2/users?page_number=1&page_size=30&status=active');
        req1.setHeader('authorization', 'Bearer '+access_token);
        req1.setHeader('content-type', 'application/json');
        
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('User List Avinash'+resl.getBody());
        
        return null;
    }*/
    
    /*public String getMeetingUUID(){
        system.debug('--consultingId--'+consultingId);
        Consulting__c c = consultingMap.get(consultingId);
        system.debug('**Access Token**'+access_token);
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        system.debug('--c--'+c+'--consultingMap--'+consultingMap);
        //req1.setEndpoint('https://api.zoom.us/v2/past_meetings/KtvONKn6Tqat43IZ3PcSAA==');
        req1.setEndpoint('https://api.zoom.us/v2/meetings/'+c.meetingId__c);
        //req1.setEndpoint('https://api.zoom.us/v2/meetings/93026049290');
        req1.setHeader('content-type', 'application/json');
        req1.setHeader('Authorization', 'Bearer '+access_token);
        //String messageBody = '{ \"topic\": \"'+meetingTitle+'\", \"schedule_for\": \"'+c.User__r.Email+'\", \"start_time\": \"'+c.From_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss.SS\'Z\'')+'\", \"duration\": '+meetingDuration+',  \"timezone\": \"'+Physicianstimezone+'\", \"password\": \"'+meetingPassword+'\"}';
        //System.debug('messageBody Avinash'+messageBody);
        //req1.setBody(messageBody);
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        if(!Test.isRunningTest()){
            HttpResponse resl = h2.send(req1);
            System.debug('Meeting Avinash'+resl.getBody());
            system.debug('Response code: '+resl.getStatusCode());
            JSONParser parserD = JSON.createParser(resl.getBody());
            system.debug('--response--'+resl+'--'+resl.getStatus());
            Integer statusCode  = resl.getStatusCode();
            system.debug('Avinash parserD >> '+ parserD);
            
            meetingObj = Zoom_MeetingObject.parse( resl.getBody());
            system.debug('Meeting info >> '+meetingObj);
            system.debug('--meetingObj uuid--'+meetingObj.uuid);
            return meetingObj.uuid;
        }else{
            return null;
        }       
    }*/  
    
    public PageReference getMeetingDetails(){
        
        Consulting__c c = consultingMap.get(consultingId);
        //String meetingUUid = getMeetingUUID();
        system.debug('--In getMeetingDetails Method--'+access_token);
        if(access_token==null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please click Update Meeting Details tab again to refresh the token'));
            return null;
        }
        //system.debug('--getMeetingUUID--'+meetingUUid);
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        req1.setEndpoint('https://api.zoom.us/v2/past_meetings/'+c.Zoom_Meeting_UUID__c);
        //req1.setEndpoint('https://api.zoom.us/v2/past_meetings/KtvONKn6Tqat43IZ3PcSAA==');
        //https://api.zoom.us/v2/past_meetings/KtvONKn6Tqat43IZ3PcSAA==
        //req1.setEndpoint('https://api.zoom.us/v2/meetings/93650752067');
        req1.setHeader('content-type', 'application/json');
        req1.setHeader('Authorization', 'Bearer '+access_token);
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        if(!Test.isRunningTest()){  
            HttpResponse resl = h2.send(req1);
            System.debug('Meeting Avinash1'+resl.getBody());
            system.debug('Response code1: '+resl.getStatusCode());
            JSONParser parserD = JSON.createParser(resl.getBody());
            system.debug('--response--'+resl.getStatus());
            Integer statusCode  = resl.getStatusCode();
            system.debug('Avinash parserD 1>> '+ parserD);
            
            meetingObj = Zoom_MeetingObject.parse(resl.getBody());
            system.debug('Meeting info >> '+meetingObj);
            system.debug('--meetingObj uuid--1'+meetingObj.uuid);
        }else{
            String JSONContent ='{"created_at": "2019-09-05T16:54:14Z","duration": 60,"host_id": "AbcDefGHi","id": 1100000,"join_url": "https://zoom.us/j/1100000","settings": {"alternative_hosts": "","approval_type": 2,"audio": "both","auto_recording":"local","close_registration": false,"cn_meeting": false,"enforce_login":false,"enforce_login_domains":"","global_dial_in_countries": ["US"],"global_dial_in_numbers": [{"city":"New York","country": "US","country_name": "US","number": "+1 1000200200","type": "toll"},{"city": "San Jose","country": "US","country_name": "US","number": "+1 6699006833","type": "toll"},{"city": "San Jose","country":"US","country_name": "US","number": "+1 408000000","type": "toll"}],"host_video": false,"in_meeting":false,"join_before_host": true,"mute_upon_entry": false,"participant_video":false,"registrants_confirmation_email": true,"use_pmi": false,"waiting_room": false,"watermark":false,"registrants_email_notification": true},"start_time": "2019-08-30T22:00:00Z","start_url":"https://zoom.us/s/1100000?iIifQ.wfY2ldlb82SWo3TsR77lBiJjR53TNeFUiKbLyCvZZjw","status": "waiting","timezone":"America/New_York","topic": "API Test","type": 2,"uuid": "ng1MzyWNQaObxcf3+Gfm6A==","schedule_for":"","total_minutes":10,"user_name":"","user_email":"","end_time":"","participants_count":2,"encrypted_password":"","h323_password":"","password":"","pstn_password":""}';       
            meetingObj = Zoom_MeetingObject.parse(JSONContent);
        }
        if(meetingObj.uuid == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Meeting does not happend'));
        }else{    
            c.Meeting_duration_min__c = meetingObj.duration;
            c.Meeting_details_updated__c = true;
            update c;
            consultingMap.remove(c.Id);
            consultingList = consultingMap.values();
        }
        return null;
    }
}