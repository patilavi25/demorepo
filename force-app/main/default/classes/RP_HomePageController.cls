public with sharing class RP_HomePageController {
    public List<Visit__c> lstVisit{get;set;}
    public List<Lab_Report__c> lstLabReport{get;set;}
    public List<Task> lstTask{get;set;}
    public List<Slot__c> lstTodaySlots  {get;set;}
    public List<Slot__c> lsttomorrowOnwardSlots  {get;set;}
    public List<Consulting__c> todaysConsulting {get;set;}
    public string consultingRecordId{get;set;}
    
    public RP_HomePageController () {
        getMyUnviewedReports();
        getInbox();
        getScheduledConsultations();
    }
    
    public void getMyUnviewedReports() {
        lstLabReport = new List<Lab_Report__c>();
        lstLabReport = [Select RecordType.Name, Patient__r.Patient_ID__c,CreatedDate, Patient__r.Name, Name from Lab_report__c 
                        where reviewed__c =: false]; 
    }
    
    public void  getInbox() {
        lstTask = [Select t.Who.Name, t.WhatId, t.Subject, t.Status, t.Priority, t.OwnerId, t.IsRecurrence, t.IsClosed,
                   t.owner.Name, t.ActivityDate,
                   CreatedBy.Name From Task t
                   where t.OwnerId =: userinfo.getuserId() and isClosed=false ];
    }
    
    public void  getScheduledConsultations() {
        Date tToday = Date.Today();
        Date SeventhDayfromToday = Date.Today()+7;
        DateTime currentTime = DateTime.now();
        todaysConsulting = [Select Id, Name,visit__r.Patient__r.Name, Slot__r.Start_Time__c, Slot__r.End_Time__c, CreatedBy.Name, visit__r.Name,User__r.Name,
                            createdby.BlueJeansMeetingPersonalId__c from Consulting__c where Slot__r.Availability__r.User__c =: userinfo.getuserId() 
                            and visit__r.Patient__c != null and  Slot__r.Start_Time__c != null  and slot__r.cancelled__c = false
                            and DAY_ONLY(convertTimezone(Slot__r.Start_Time__c))  >=: tToday and DAY_ONLY(convertTimezone(Slot__r.Start_Time__c)) <=:SeventhDayfromToday  
                            and Slot__r.Start_Time__c >=: currentTime Order by Slot__r.Start_Time__c];
    }
    
    public pagereference reScheduleConsulting() {
        List<Consulting__c > lstConsulting = [Select Slot__c from Consulting__c where id =: consultingRecordId];
        if(lstConsulting.size() > 0 ) {
            Slot__c slot = new Slot__c ( id = lstConsulting[0].Slot__c ) ;
            slot.cancelled__c = true ;
            update slot ; 
        }
        getScheduledConsultations();
        return null ;
    }
}