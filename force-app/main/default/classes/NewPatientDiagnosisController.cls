public class NewPatientDiagnosisController {
public Patient_Diagnosis__c pd {get;set;}
    
    public NewPatientDiagnosisController(ApexPages.StandardController stdController) {
        if(pd == null){
            pd = new Patient_Diagnosis__c();
            pd.Patient__c = apexpages.currentpage().getparameters().get('contactId');
        }
    }
    
    public PageReference save(){
        insert pd;
        PageReference orderPage = new PageReference('/' + pd.Patient__c);
        orderPage.setRedirect(true);
        return orderPage;
    } 
    public PageReference Cancel(){
        PageReference orderPage = new PageReference('/' + pd.Patient__c);
        orderPage.setRedirect(true);
        return orderPage;
    } 
}