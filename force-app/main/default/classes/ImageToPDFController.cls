public class ImageToPDFController {

    public string imageURL {get;set;}
    public ImageToPDFController(Apexpages.StandardController con){
        Id recordId = con.getId();
        List<Attachment> attachmentList = [SELECT Id from Attachment WHERE ParentId=:recordId AND Name='Signature Image' LIMIT 1];
        if(!attachmentList.isEmpty())
            imageURL = '/servlet/servlet.FileDownload?file=' + attachmentList[0].Id;
    }
}