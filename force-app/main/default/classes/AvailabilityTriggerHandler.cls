public class AvailabilityTriggerHandler {
    public  void  afterInsert_Availability( List<Availability__c >  lstAvail ) {
        Integer DayDifference = 0 ;
        Date startDate = lstAvail[0].Start_Date__c ; 
        Date EndDate  = lstAvail[0].End_Date__c ;
        DateTime StartDateTime = (DateTime)startDate ;
        DateTime EndDateTime =   (DateTime)EndDate  ;
        StartDateTime  = StartDateTime.addSeconds(20);
        EndDateTime  = EndDateTime.addSeconds(20);
        Map<String, List<Date>> mapDaytoDate = new Map<String, List<Date>>();
        if(lstAvail.size()  == 1) {
            DayDifference  =  startDate.daysBetween(EndDate)  ;
            StartDateTime = StartDateTime+1 ;
            //Iterate over dates and find out weekdays names - 
            for( Integer i =  0 ; i < DayDifference+1 ; i++  ) {
                String dayOfWeek = StartDateTime.format('E');
                //IF DAY MATCH WITH MENTIONED ONE in Availability - 
                if( (lstAvail[0].Sunday__c == true &&   dayOfWeek == 'Sun') || 
                   (lstAvail[0].Monday__c == true &&   dayOfWeek == 'Mon') ||
                   (lstAvail[0].Tuesday__c == true &&   dayOfWeek == 'Tue' )  ||
                   (lstAvail[0].Wednesday__c == true &&   dayOfWeek == 'Wed' ) ||
                   (lstAvail[0].Thrusday__c == true &&   dayOfWeek == 'Thu' )||
                   (lstAvail[0].Friday__c == true &&   dayOfWeek == 'Fri') ||
                   (lstAvail[0].Saturday__c == true &&   dayOfWeek == 'Sat')) {
                       mapDaytoDate = getDateListsRecordsForDay(mapDaytoDate, dayOfWeek, StartDateTime);
                   }
                StartDateTime = StartDateTime+1;
            }
        }
        Integer[] fromTimeArray  = getTimeStringArray(lstAvail[0].From_Time__c);
        Integer[] ToTimeArray    = getTimeStringArray(lstAvail[0].To_Time__c );
        List<Slot__c> MasterListofSlots  = new List<Slot__c>();
        for( String varKey  : mapDaytoDate.keyset() ) {
            for( Date d   : mapDaytoDate.get(varKey)) {
                List<Slot__c> lst = ConstructDateTime( lstAvail[0]  , d, fromTimeArray , ToTimeArray);
                MasterListofSlots.addAll(lst);  
            }
        }
        if(MasterListofSlots.size() > 0 ) {
            insert MasterListofSlots;
        }
    }
    
    public Map<String, List<Date>>  getDateListsRecordsForDay( Map<String, List<Date>> currentMap , string dayofWeek, DateTime currentDateTime ) {
        if(currentMap.get(dayofWeek) != null) {
            List<Date> lstDate = currentMap.get(dayofWeek) ; 
            lstDate.add(currentDateTime.Date());
            currentMap.put(dayofWeek, lstDate);
        } else {
            List<Date> lstDate = new List<Date>() ; 
            System.debug('*****currentDateTime.Date()********'+currentDateTime.Date());
            lstDate.add(currentDateTime.Date());
            currentMap.put(dayofWeek, lstDate);
        }
        return currentMap;          
    }
    
    public  List<Slot__c> ConstructDateTime( Availability__c avail ,  Date d, Integer[] fromTimeArray , Integer[] ToTimeArray) {
        DateTime  fromDateTime  =  DateTime.newInstance( d.year(), d.month(), d.day(), fromTimeArray[0], fromTimeArray[1] , 0 );
        DateTime  toDateTime  =  DateTime.newInstance(d.year(), d.month(), d.day(), ToTimeArray[0], ToTimeArray[1], 0 );
        List<Slot__c> lstSlots = new List<Slot__c>();
        do {
            if(fromDateTime < toDateTime) {
                Slot__c s  = new Slot__c();
                s.Start_Time__c = fromDateTime ;
                s.End_time__c =   fromDateTime.addMinutes(15) ;
                if(s.End_time__c > toDateTime ) {
                    s.End_time__c = toDateTime;
                }
                s.Availability__c = avail.Id;
                lstSlots.add(s);
                fromDateTime = s.End_time__c;
            }
        } while ( fromDateTime < toDateTime);
        return lstSlots;
    }
    
    public Integer[] getTimeStringArray(String tTime ) {
        Integer[] timeArray = new  Integer[]{};
            String[] LclTimeArray  = tTime.Split(' ');
        String[] strTimeDigits = LclTimeArray[0].Split(':');
        if( LclTimeArray[1].equalsIgnorecase('AM')  ) {
            timeArray.add(Integer.valueof(strTimeDigits[0]));
            timeArray.add( Integer.valueof(strTimeDigits[1]));   
        } 
        if(LclTimeArray[1].equalsIgnoreCase('PM')) {
            timeArray.add( Integer.valueof(strTimeDigits[0])+12);
            timeArray.add(Integer.valueof(strTimeDigits[1]));  
        }
        return timeArray;
    }
}