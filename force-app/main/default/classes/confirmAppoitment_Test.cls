@isTest
public class confirmAppoitment_Test {
    static testMethod  void setup() {
        //Create a Patient
        Contact p = new Contact();
        p.LastName = 'Patient';
        p.Gender__c = 'Male';
        p.Birthdate = system.today();
        insert p ;
        
        //Create a Visit
        Visit__c v = new Visit__c (Check_In_time__c = system.now() + 100, Onsite_Physician__c = UserInfo.getUserId(), Patient__c = p.Id, 
                                   ReasonforVisit__c = 'Sick');
        insert v;
        
        //Create a SOAP Note
        SOAP_Note__c sn = new SOAP_Note__c (Soap_Assessment__c = 's', Soap_Objective__c = 's', Soap_Plan__c = 's', Subjective__c = 's', Visit__c = v.Id);
        insert sn;
        
        //Create Availabilities for Remote Physician
        Availability__c av = new Availability__c(User__c = UserInfo.getUserId(), Start_Date__c = system.today()+1, End_Date__c = system.today()+2, 
                                                 From_Time__c = '00:15 AM', To_Time__c = '11:30 PM', Sunday__c = True, Monday__c = True,
                                                 Tuesday__c = True, Wednesday__c = True, Thrusday__c = True, Friday__c = True, Saturday__c = True);
        insert av;
        
        //Query Slots
        List<Slot__c> slot = new List<Slot__c>([Select Id, Availability__c, Physician__c, Start_Time__c, End_Time__c, Is_Booked__c, Is_Future_Slot__c, Cancelled__c  from Slot__c order by End_Time__c Desc limit 1]);
        system.debug('Slots - '+slot);
        //Create Consulting Record
        Consulting__c con = new Consulting__c(User__c = UserInfo.getUserId(), Reason__c = 'Sick', Slot__c = slot[0].Id, meetingId__c = '93026049290', isConfirmed__c=false, Meeting_details_updated__c = false, Start_Time__c = slot[0].Start_Time__c, Visit__c= v.Id );
        insert con;
    }
    
    static testMethod void PageReference_Test() {    
        confirmAppoitment ca = new confirmAppoitment();
        PageReference pg = ca.ZoomAuth();
        
    }
    static testMethod void AccessToken_Test(){
        confirmAppoitment ca1 = new confirmAppoitment();
        ca1.AccessToken();      
    }
    static testMethod void getMeetingUUID(){
        setup();
        Consulting__c con = [Select id From Consulting__c limit 1];
        confirmAppoitment ca1 = new confirmAppoitment();
        ca1.consultingId=con.Id;
        ca1.getMeetingUUID('testId');
        //ca1.getMeetingDetails();     
    }
    static testMethod void setMeeting(){
        setup();
        Consulting__c con = [Select id, From_Time__c, Patients_Email__c, User__c, User__r.email, User__r.Name, Patient__c, User__r.TimeZoneSidKey, HangoutURL__c, Zoom_Meeting_Password__c From Consulting__c limit 1];        
        confirmAppoitment ca1 = new confirmAppoitment();
        ca1.consultingId=con.Id;
        ca1.access_token='ewedfghjbnm43521';
        ca1.setMeeting();
    }
    static testMethod void setMeeting2(){
        setup();
        Consulting__c con = [Select id, From_Time__c, Patients_Email__c, User__c, User__r.email, User__r.Name, Patient__c, User__r.TimeZoneSidKey, HangoutURL__c, Zoom_Meeting_Password__c From Consulting__c limit 1];        
        confirmAppoitment ca1 = new confirmAppoitment();
        ca1.consultingId=con.Id;
        ca1.setMeeting();
    }
    static testMethod void sendEmail(){
        setup();
        Consulting__c con = [Select id, From_Time__c, Patients_Email__c, User__c, User__r.email, User__r.Name, Patient__c, User__r.TimeZoneSidKey, HangoutURL__c, Zoom_Meeting_Password__c From Consulting__c limit 1];        
        confirmAppoitment ca1 = new confirmAppoitment();
        confirmAppoitment.sendMail(con);
    }
}