public class AdminHomeCon
{
	public List<Visit__c> lstVisit {get; set;}
    public List<Lab_Report__c> lstLabReport {get; set;}
    public Date fromDate {get;set;}
    //public Date toDate {get;set;}
    public List<Visit__c> visitLst{get;set;}
    public List<Consulting__c> todaysConsulting {get;set;}
    
    public AdminHomeCon()
    {
        fromDate = Date.today().addDays(-7);
        //toDate = Date.today();
        getAllPatientQueue();
        getMyUnviewedReports();
        getAllPatientListOnSelecteddate();
        getScheduledConsultations();
    }
    
    public void getAllPatientQueue()
    {
        lstVisit = new List<Visit__c>();
        List<Visit__c> lstlclVisit = [Select Id, Name,Patient__r.Name, Patient__r.Patient_ID__c, Check_In_time__c, 
                                          Patient__r.Account.Name, Onsite_Physician__r.Name, Onsite_Physician__c, 
                                          (Select Id from SOAP_Notes__r Limit 1) 
                                          From Visit__c 
                                          Where DAY_ONLY(convertTimezone(Check_In_time__c)) >=: Date.today() Order By Check_In_time__c ASC ]; 
        for( Visit__c v : lstlclVisit){
            if(v.SOAP_Notes__r.size() == 0 ) {
                lstVisit.add(v);
            }
        }
    }
    
    public void getAllPatientListOnSelecteddate(){
        visitLst = new List<Visit__c>();
        for( Visit__c v : [Select Id, Name,Patient__r.Name, Patient__r.Patient_ID__c, Check_In_time__c, 
                                      Patient__r.Account.Name, Onsite_Physician__r.Name, Onsite_Physician__c, 
                                      (Select Id from SOAP_Notes__r Limit 1) 
                                      from Visit__c Where DAY_ONLY(convertTimezone(Check_In_time__c)) =: date.today() 
                                      Order By Check_In_time__c DESC]){
            if(v.SOAP_Notes__r.size() > 0 ) {
                visitLst.add(v);
            }
        }
        for(Visit__c v : [Select Id, Name,Patient__r.Name, Patient__r.Patient_ID__c, Check_In_time__c, 
                                      Patient__r.Account.Name, Onsite_Physician__r.Name, Onsite_Physician__c, 
                                      (Select Id from SOAP_Notes__r Limit 1) 
                                      from Visit__c Where DAY_ONLY(convertTimezone(Check_In_time__c)) >=: fromDate 
                                      and DAY_ONLY(convertTimezone(Check_In_time__c)) <: date.today() Order By Check_In_time__c DESC]){
                                         visitLst.add(v); 
                                      }    
    }
    
    public void getMyUnviewedReports()
    {
        lstLabReport = new List<Lab_Report__c>();
        lstLabReport = [Select RecordType.Name, Patient__r.Patient_ID__c,CreatedDate, Patient__r.Name, Name 
                        	From Lab_report__c 
                        	Where reviewed__c =: false];
    }
    
    public void  getScheduledConsultations() {
        Date tToday = Date.Today();
        Date SeventhDayfromToday = Date.Today()+7;
        DateTime currentTime = DateTime.now();
        todaysConsulting = [Select Id, Name,visit__r.Patient__r.Name, Slot__r.Start_Time__c, HangoutURL__c, Slot__r.End_Time__c, CreatedBy.Name, visit__r.Name,User__r.Name,
                            createdby.BlueJeansMeetingPersonalId__c from Consulting__c where User__c =: userinfo.getuserId() 
                            and visit__r.Patient__c != null and  Slot__r.Start_Time__c != null and isConfirmed__c = true
                            and DAY_ONLY(convertTimezone(Slot__r.Start_Time__c))  >=: tToday  
                            and Slot__r.Start_Time__c >=: currentTime and slot__r.cancelled__c = false Order by Slot__r.Start_Time__c  ];
    }
}