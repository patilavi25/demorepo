/*
    Created by: Parimal Sharma
    Last Update: 13 September 2018 by Greg Hacic
    
    Notes:
        - controller for the VitalsEditRedirect.page
*/
public class VitalsEditRedirect {
    
    private final Vitals__c vitals; //variable for the Contact object
    
    //constructor
    public VitalsEditRedirect(ApexPages.StandardController standardPageController) {
        vitals = (Vitals__c)standardPageController.getRecord(); //initialize the standard controller
    }
    
    //method invoked from the Visualforce pages action attribute
    public PageReference redirectToAnotherVisualforcePage() {
        Vitals__c recordDetails = [SELECT Id FROM Vitals__c WHERE Id = :vitals.Id]; //query for the vitalsId
        
        PageReference returnPage = Page.NewVitals; //construct the PageReference for the page to which we are redirecting
        returnPage.getParameters().put('retURL','/'+recordDetails.Id); //set the retURL parameter in order to allow for returning to Vitals if User cancels from page
        returnPage.getParameters().put('id',recordDetails.Id); //set the Id of the Account record
        returnPage.getParameters().put('edit','true'); //set the Id of the Account record
        returnPage.setRedirect(true); //indicate that the redirect should be performed on the client side
        return returnPage; //send the User on their way
    }

}