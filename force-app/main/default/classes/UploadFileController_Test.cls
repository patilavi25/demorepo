@isTest
public class UploadFileController_Test {
	@isTest
    public static void testWatermark(){
        
        Blob AttchBody = Blob.valueof('testContent');
        Document doc = new Document();
        doc.Name = 'Watermark Image';
        //doc.DeveloperName = 'Watermark_Image';
        doc.Body = AttchBody;
        doc.ContentType = 'image/png';
        doc.Type = 'png';
        doc.IsPublic = true;
        doc.AuthorId = UserInfo.getUserId();
        doc.FolderId = '00lj0000000ztvj';
        insert doc;
        
        UploadFileController ufc = new UploadFileController();
        ufc.AttchName = 'test.png';
        ufc.AttchBody = AttchBody;
        ufc.UploadWatermark();
    }
    @isTest
    public static void testSignature(){
        
        Blob AttchBody = Blob.valueof('testContent');        
        UploadFileController ufc = new UploadFileController();
        ufc.AttchName = 'test.png';
        ufc.AttchBody = AttchBody;
        ufc.UploadWatermark();
        ufc.UploadSignature();
    }
    
    @isTest
    public static void testNegative(){ 
        User currentuser=new User();
        currentuser=[Select Id,Name,Email,Signature__c from User where Id=:userinfo.getuserId()];
        Physician_Info__c physician =new Physician_Info__c();
        physician.Name = currentuser.Name;
        physician.Physician__c = currentuser.Id;
        physician.SignatureURL__c ='www.google.com';
        insert physician;
        UploadFileController ufc = new UploadFileController();
        ufc.UploadWatermark();
        ufc.UploadSignature();
    }
}