public without sharing class CaptureSignatureController {
    
    public Id recordId{get;set;}
    public user currentuser{get;set;}
    
    public CaptureSignatureController(ApexPages.StandardController stdController){
        recordId = stdController.getId();
        currentuser=new User();
        currentuser=[Select Id,Name,Email from User where Id=:userinfo.getuserId()];
    }
    
    @RemoteAction
    public static String saveSignature(String imageUrl, String recordId) {
        
        try {
            User currentuser=new User();
        	currentuser=[Select Id,Name,Email,Signature__c from User where Id=:userinfo.getuserId()];
            
            List<Physician_Info__c> physicianInfo = new List<Physician_Info__c>();
            physicianInfo = [Select id, Name, Physician__c, SignatureURL__c, WatermarkImageURL__c, OwnerId From Physician_Info__c Where Physician__c=:userinfo.getuserId() limit 1];
            
            if(physicianInfo.size() < 1){
                Physician_Info__c physician =new Physician_Info__c();
                physician.Name = currentuser.Name;
                physician.Physician__c = currentuser.Id;
                insert physician;
                physicianInfo.add(physician);
            }
            
            String fileName = 'Signature '+currentuser.Name+'.png'; 
            List<Attachment> attachmentList = [SELECT Id from Attachment WHERE ParentId=:physicianInfo[0].Id AND Name=:fileName];
            Attachment accSign = new Attachment();
            accSign.ParentID = physicianInfo[0].Id;
            //accSign.ParentID = '005j000000BkJiU';	
            accSign.Body = EncodingUtil.base64Decode(imageUrl);
            accSign.contentType = 'image/png';
            accSign.Name = 'Signature '+currentuser.Name+'.png';
            accSign.OwnerId = UserInfo.getUserId();
            //insert accSign;
            
            /*Account acc = [Select id, Name From Account where Name = 'Signature'];
            String fileName = 'Signature '+currentuser.Name+'.png'; 
            List<Attachment> attachmentList = [SELECT Id from Attachment WHERE ParentId=:acc.Id AND Name=:fileName];
            Attachment accSign = new Attachment();
            
            accSign.ParentID = acc.Id;
            //accSign.ParentID = '005j000000BkJiU';	
            accSign.Body = EncodingUtil.base64Decode(imageUrl);
            accSign.contentType = 'image/png';
            accSign.Name = 'Signature '+currentuser.Name+'.png';
            accSign.OwnerId = UserInfo.getUserId();
            insert accSign;*/
            
            //Get attachment
            /*Attachment attach = [SELECT Id, Name, OwnerId, Body, ContentType, ParentId From Attachment Where Name='Signature Image' LIMIT 1];
            
            //Insert ContentVersion
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
            cVersion.PathOnClient = 'Signature Image1.png';//File name with extention
            cVersion.Origin = 'C';//C-Content Origin. H-Chatter Origin.
            cVersion.OwnerId = UserInfo.getUserId();//Owner of the file
            cVersion.Title = 'Signature Image1';//Name of the file
            cVersion.VersionData = EncodingUtil.base64Decode(imageUrl);//File content
            upsert cVersion;
            
            //After saved the Content Verison, get the ContentDocumentId
            Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
            system.debug('conDocumentId: '+conDocument);
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink =[Select ContentDocumentId, LinkedEntityId From ContentDocumentLink where ContentDocumentId=: conDocument limit 1];
            if(cDocLink == null){
                system.debug('Create new cDocLink: ');
                cDocLink = new ContentDocumentLink();
                cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
                cDocLink.LinkedEntityId = UserInfo.getUserId();//Add attachment parentId
                cDocLink.ShareType = 'C';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
                cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
                upsert cDocLink;
            }
            system.debug('--conVer--'+cVersion+' --conDocLink--'+cDocLink);
            */
            if(attachmentList.size() > 0){
                //delete attachmentList; 
            }
            String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();
            //String signatureURL = sfdcURL+'/servlet/servlet.FileDownload?file=' + accSign.Id;
            
            //update signature URL on user recod
           	//physicianInfo[0].SignatureURL__c = signatureURL;
            //update physicianInfo;
            
            PageReference pdf = Page.ImageAsPDF;
            String docId;
            List<Document> documentList = [SELECT Id, Name, DeveloperName, Body, ContentType, Type, IsPublic, AuthorId, FolderId from Document WHERE DeveloperName= 'Watermark_Image1'];
            if(documentList.size()>0){
                documentList[0].Body = EncodingUtil.base64Decode(imageUrl);
                documentList[0].ContentType = 'image/png';
                documentList[0].IsPublic = true;
                update documentList;
                docId = documentList[0].Id;
            }else{
                Document doc = new Document();
                doc.Name = 'Watermark Image1';
                doc.DeveloperName = 'Watermark_Image1';
                doc.Body = EncodingUtil.base64Decode(imageUrl);
                doc.ContentType = 'image/png';
                doc.IsPublic = true;
                doc.AuthorId = UserInfo.getUserId();
                doc.FolderId = '00lj0000000ztvj';
                insert doc;
                docId = doc.Id;
            }
   
            return '/servlet/servlet.FileDownload?file=' + docId;
     
        }catch(Exception e){
            system.debug('---------- ' + e.getMessage());
            return JSON.serialize(e.getMessage());
        }
    }
    
}