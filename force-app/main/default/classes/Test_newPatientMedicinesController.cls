@IsTest
public class Test_newPatientMedicinesController {
    public static testmethod void NewPatientMedicine() {
        // Put Id into the current page Parameters
        
        Contact patient = new Contact();
        patient.LastName = 'Patient';
        patient.Gender__c = 'Male';
        patient.Birthdate = system.today();
        insert patient ;
        
        Medicine_List__c ml = new Medicine_List__c();
        ml.Name = 'Vicks';
        ml.Formulation__c='test';
        ml.Strength__c='test';
        insert ml;
        
        Patient_Medicine__c pm = new Patient_Medicine__c();
        pm.Medicine_List__c = ml.Id;
        pm.Patient__c=patient.Id;
        
        ApexPages.StandardController beforeController = new ApexPages.StandardController(pm);
        newPatientMedicinesController beforeExtension = new newPatientMedicinesController(beforeController);
        ApexPages.StandardController controller = new ApexPages.StandardController(pm);
        ApexPages.currentPage().getParameters().put('contactId', patient.Id);
        
        Test.startTest();
        newPatientMedicinesController extension = new newPatientMedicinesController(controller);
        extension.pm = pm;
        extension.save();
        extension.Cancel();
        Test.stopTest();
    }
}