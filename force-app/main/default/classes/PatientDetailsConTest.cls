@isTest
public class PatientDetailsConTest
{
	public static testmethod void testPatientDetails()
    {
        //Create a Patient
        Contact p = new Contact();
        p.LastName = 'Patient';
        insert p ;
        
        //Create a Visit
        Visit__c visit = new Visit__c (Check_In_time__c = system.now(), Onsite_Physician__c = UserInfo.getUserId(), Patient__c = p.Id, 
                                   ReasonforVisit__c = 'Sick');
        insert visit;
        
        Test.setCurrentPageReference(new PageReference('Page.PatientDetails'));
        System.currentPageReference().getParameters().put('Id', visit.Id);
        PatientDetailsCon patientDetailsCon = new PatientDetailsCon();
        patientDetailsCon.openPrint();
        patientDetailsCon.DateOfVisit = Datetime.now();
        patientDetailsCon.ReasonOfVisit = 'fewer';
        patientDetailsCon.ShowAllergy = true;
        patientDetailsCon.ShowMedicine = true;
    }
}